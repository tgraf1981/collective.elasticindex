var en_includes = {
    search_results: '<h3>Search results for <span id="search-query">{{ query }}</span></h3>',
    num_docs_found: '{{ num_rows }} documents found',
    details: 'Details'
};

var de_includes = {
    search_results: '<h3>Suchresultate für <span id="search-query">{{ query }}</span></h3>',
    num_docs_found: '{{ num_rows }} Dokumente gefunden',
    details: 'Details'
};

var all_includes = {
    en: en_includes,
    de: de_includes
};

var LANG_TO_ES = {
    'ar': 'arabic',
    'eu': 'basque',
    'br': 'brazilian',
    'bg': 'bulgarian',
    'cn': 'chinese',
    'da': 'danish',
    'nl': 'dutch',
    'en': 'english',
    'fi': 'finnish',
    'fr': 'french',
    'de': 'german',
    'gr': 'greek',
    'hi': 'hindi',
    'hu': 'hungarian',
    'id': 'indonesian',
    'ga': 'irish',
    'it': 'italian',
    'lv': 'latvian',
    'no': 'norwegian',
    'fa': 'persian',
    'po': 'portuguese',
    'ro': 'romanian',
    'ru': 'russian',
    'ku': 'sorani',
    'es': 'spanish',
    'sv': 'swedish',
    'tr': 'turkish',
    'th': 'thai'
};


/* Render Markup.js template with ``params``
 * taking the current language into account including
 * some variables from Plone content.
 */

function render_markup(template, params) {

    /* Include additional variables into Markup.js */
    if (CURRENT_LANGUAGE in all_includes) 
        var includes = all_includes[CURRENT_LANGUAGE]
    else
        var includes = all_includes['en']

    includes['portal_url'] = PORTAL_URL;
    includes['context_url'] = CONTEXT_URL;
    Mark.includes = includes;
    return Mark.up(template, params)
}

// Hang on in there. We have jQuery, 1.4.
var BATCH_SIZE = 15;
var ALLOWED_COLON_WORDS = /(title|description|contributors|subject|content|author|language)$/i;

// Lucene special chars: && || ! ( ) { } [ ] ^ \
// Following regex contains Lucene special chars and punctuation chars (the colon it's treated separately).
// Chars with a "meaning" in regex syntax have been escaped.
var DISALLOWED_CHARS = /(&|\||!|\(|\)|\{|\}|\[|\]|\^|\\|;|,|\.|\/|<|>)/g;

var remove_colons_from_query = function(query) {
    var origin = query.split(':'),
        cleaned = [],
        i, match, len;
    for (i=0, len=origin.length; i <len; i++) {
        match = origin[i].search(ALLOWED_COLON_WORDS);
        if (match > -1) {
            cleaned.push(origin[i].substring(0, match));
            cleaned.push(origin[i].substring(match).toLowerCase());
            cleaned.push(':');
        } else {
            cleaned.push(origin[i]);
        }
    }
    return cleaned.join('');
};

var validate_year = function(year) {
    if (year.match(/^\d{4}$/)) {
        return year;
    };
    return null;
};

var ElasticSearch = function($form) {
    var search_urls = $form.data('server-urls');
    // List of callbacks during the various search stages.
    var start_query = [],
        empty_results = [],
        results = [];
    // Private variables.
    var previous_query = null;

    var get_url = function() {
        // Return an URL where to post the search query.
        return search_urls[Math.floor(Math.random() * search_urls.length)];
    };

 var build_query = function(original) {
        // Build the search query out of the the collect data.
        var queries = [],
            filters = [],
            query = null,
            sort = {};
        // Sorting options.
        if (original.sort == 'created' || original.sort == 'modified') {
            sort[original.sort] = 'desc';
        } else if (original.sort == 'title') {
            sort = "sortableTitle";
        } else {
            sort = "_score";
        };

        var filter = [];
        query = {
            "bool": {
                "must": [
                    {
                        query_string: {
                            query: remove_colons_from_query(original.term.replace(DISALLOWED_CHARS, ' ')),
                            default_operator: "AND",
                            fields: [
                                "title^3",
                                "contributors^2",
                                "subject^2",
                                "description",
                                "content"
                            ]
                        }
                    }
                ],
                "filter" : filter
            }
        };

        var result = {
            size: BATCH_SIZE,
            sort: [sort],
            highlight: {
                fields: {
                    title: {number_of_fragments: 1, fragment_size: 150},
                    description: {fragment_size: 150, number_of_fragments: 2},
                    content: {fragment_size: 150, number_of_fragments: 2}
                }
            },
            stored_fields: ['url', 'title', 'description', 'metaType', 'metaTypeName', 'author', 'authorName', 'contributors', 'modified', 'subject', 'review_state', 'language']
        };
        if (query !== null) {
            result['query'] = query;
        }
        ;
        return result;


    }





    var do_search = function(query, from) {
        // Post the search query to the server.
        query['from'] = from || 0;

        var ts = (new Date).getTime()
        $.ajax({
            url: get_url(),
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            success: function(data) {

                var diff = (new Date).getTime() - ts;

                var notifies = data.hits.total ? results : empty_results;
                for (var i=0, len=notifies.length; i < len; i++) {
                    notifies[i](data.hits, from || 0);
                }
            },
            error: function () {
                var diff = (new Date).getTime() - ts;

                for (var i=0, len=empty_results.length; i < len; i++) {
                    empty_results[i]();
                }
            },
            data: JSON.stringify(query)
        });
        return query;
    };

    return {
        subscribe: function(plugin) {
            if (plugin.onempty !== undefined) {
                empty_results.push(plugin.onempty);
            };
            if (plugin.onresult !== undefined) {
                results.push(plugin.onresult);
            };
            if (plugin.onstart !== undefined) {
                start_query.push(plugin.onstart);
            };
        },
        scroll: function(index) {
            if (previous_query !== null) {
                do_search(previous_query, index);
            };
        },
        search: function(query) {
            for (var i=0, len=start_query.length; i < len; i++) {
                start_query[i](query);
            };
            previous_query = do_search(build_query(query));
        }
    };
};

var SummaryDisplayPlugin = function($summary) {
    var $term = $summary.find('span#esSearchTerm'),
        $location = $summary.find('span#esSearchLocation span');

    return {
        onstart: function(query) {
            $term.text('"' + query.term + '"');
            if (query.url) {
                $location.text('"' + query.url.replace(/^.*?:\/{2}/,'') + '"');
                $location.parent().show();
            } else {
                $location.parent().hide();
            };
        }
    };
};

var ResultHeaderDisplayPlugin = function($header) {
    var $loader = $header.find('img'),
        $message = $header.find('span.discreet'),
        $count = $header.find('span.searchResultsCount'),
        initial = true;

    return {
        onstart: function() {
            if (initial) {
                $header.show();
                initial = false;
            };
            $loader.show();
            $message.hide();
            $('#search-results-target').empty();
        },
        onempty: function() {
            $count.text('0');
            $loader.hide();
            $message.hide();
        },
        onresult: function(data) {
            $count.text(data.total);
            $loader.hide();
            $message.show();
        }
    };
};

var ResultDisplayPlugin = function($result, $empty) {
    var trailing_punctuation_re = /^[;,.\s]*([\S\s]*?)[;,.\s]*$/;

    var trim_punctuation = function(string) {
        return string;
        var match = string.match(trailing_punctuation_re);
        if (match) {
            return match[1];
        }
        return string;
    };

    var truncate_text = function(string) {
        return string.length > 256 ? string.substr(0, 256) + ' &hellip;' : string;
    };

    var get_entry = function(entry) {
        
        var title = null,
            description = null,
            content = null,
            i;

        if (entry.highlight !== undefined) {
            if (entry.highlight.title !== undefined) {
                title = entry.highlight.title[0];
            } else {
                title = entry.fields.title[0];
            };
           
            if (entry.highlight.description !== undefined) {
                description = '';
                for (i=0; i < entry.highlight.description.length; i++) {
                    description += ('<span class="highlight-item">' + trim_punctuation(entry.highlight.description[i]) + '</span>');
                };
            };
            if (entry.highlight.content !== undefined) {
                content = '';
                for (i=0; i < entry.highlight.content.length; i++) {
                    content += ('<span class="highlight-item">' + trim_punctuation(entry.highlight.content[i]) + '</span>');
                };
            };
        };
        if (description === null) {
            if (typeof entry.fields.description === "string") {
                description = trim_punctuation(truncate_text(entry.fields.description));
            } else {
                description = '';
            };
        };

         if (title === null) {
                title = entry.fields.title[0]
        };

        var author = entry.fields.author;
        var authorName = entry.fields.authorName;
        var contributors = entry.fields.contributors;
        var subject = entry.fields.subject;
        if (subject)
            subject.sort();
        var modified = new Date(entry.fields.modified);
        modified = $.sprintf('%02d.%02d.%d %02d:%02dh', modified.getDate(), modified.getMonth() + 1, modified.getFullYear(), modified.getHours(), modified.getMinutes())
        var meta_type = entry.fields.metaType[0].replace(/^\s+|\s+$/g, '').replace(/\s+/g,'-').toLowerCase();
        var meta_type_name = entry.fields.metaTypeName;
        var review_state = entry.fields.review_state;
        var language = entry.fields.language;
        var document_url = entry.fields.url[0];
        var entry_title = $('<a></a>').attr('href', document_url).html(title).get(0).outerHTML;
        
        result = {
            title: title,
            description: description,
            content: content,
            document_url: document_url,
            entry_title: entry_title,
            meta_type: meta_type, 
            meta_type_name: meta_type_name, 
            modified: modified,
            author: author,
            authorName: authorName,
            contributors: contributors,
            review_state: review_state,
            subject: subject,
            language: language
        };
        return result;
    };

    return {
        onempty: function() {
            var template = $('#search-results-template').html();
            var query = $('#query').val();
            var sort_on = $('#sort_on').val();
            var params = {rows: [], num_rows: 0, query: query, sort_on: sort_on};
            var rendered = render_markup(template, params);
            $('#search-results-target').html(rendered);
        },
        onresult: function(data) {
            var entry, i, len;
            var rows = [];
            if (data.hits.length) {
                var template = $('#search-results-template').html();
                for (i=0, len=data.hits.length; i < len; i++) {
                    rows.push(get_entry(data.hits[i]));
                }
                var query = $('#query').val();
                var params = {rows: rows, num_rows: data.total, query: query, sort_on: sort_on};
                var rendered = render_markup(template, params);
                $('#search-results-target').html(rendered);
            }  
        }
    };
};

var BatchDisplayPlugin = function($batch, update) {
    var PAGING    = 3,
        NO_LEAP   = 0,
        PRE_LEAP  = 1,
        POST_LEAP = 2,

        $prev = $('span.previous'),
        $next = $('span.next'),

        $prev_a = $('span.previous a'),
        $next_a = $('span.next a'),

        $prev_size = $('span.previous a span span'),
        $next_size = $('span.next a span span'),

        link_to = function (opt) {
            var $item;

            if (opt.current) {
                $item = $('<span class="esGeneratedBatch current"> ' + opt.page + ' </span>');
            } else {
                $item = $('<a class="esGeneratedBatch" href=""> ' + opt.page + ' </a>');

                if (opt.leap) {
                    var $leap = $('<span>&hellip;</span>');
                    if (opt.leap == PRE_LEAP) {
                        $item.prepend($leap);
                    } else {
                        $item.append($leap);
                    }
                };

                $item.bind('click', function () {
                    update((opt.page - 1) * BATCH_SIZE);
                    return false;
                });
            };
            return $item;
        };

    return {
        onempty: function() {
            $batch.hide();
        },
        onresult: function(data, current) {
            $batch.hide();

            if (data.total <= BATCH_SIZE) {
                return;
            }

            $batch.children('.esGeneratedBatch').remove();

            var page_count = Math.ceil(data.total / BATCH_SIZE);
            var current_page = Math.ceil(current / BATCH_SIZE) + 1;
            var $current = link_to({page : current_page, current: true});

            $current.insertAfter($next);

            for (var i = PAGING; i >= 1; i--) {
                if (current_page - i <= 1) {
                    continue;
                }
                link_to({page : current_page - i}).insertBefore($current);
            };

            for (var i = PAGING; i >= 1; i--) {
                if (current_page + i >= page_count) {
                    continue;
                }
                link_to({page : current_page + i}).insertAfter($current);
            };

            if (current_page > 1) {
                var first_leap = current_page - PAGING > 2 ? POST_LEAP : NO_LEAP;
                $prev_size.text(BATCH_SIZE);
                $prev_a.unbind();
                $prev_a.bind('click', function () { update(current - BATCH_SIZE); return false });
                $prev.show();
                link_to({page: 1, leap: first_leap}).insertAfter($next);
            } else {
                $prev.hide();
            };

            if (current_page < page_count) {
                var last_leap = current_page + PAGING < (page_count - 2) ? PRE_LEAP : NO_LEAP;
                var next_size = Math.min(data.total - (current_page * BATCH_SIZE), BATCH_SIZE);
                $next_size.text(next_size);
                $next_a.unbind();
                $next_a.bind('click', function () { update(current + BATCH_SIZE); return false });
                $next.show();
                link_to({page: page_count, leap: last_leap}).appendTo($batch);
            } else {
                $next.hide();
            };

            $batch.show();
        }
    };
};

function init_search_form() {

    $('.esSearchForm').each(function() {
        var $form = $(this),

            $resultHeader = $form.find('h1.esResultHeader'),
            $searchResults = $form.find('dl.searchResults'),
            $listingBar = $form.find('div.listingBar'),
            $emptyResults = $form.find('div.emptySearchResults'),
            $summaryBox = $form.find('div#esSearchSummaryBox'),

            $options = $form.find('div.esSearchOptions'),
            search = ElasticSearch($form);

        var $query = $form.find('input.searchPage'),
            $author = $form.find('input#Contribs'),
            $subject = $form.find('select#subject'),
            $review_state = $form.find('select#review_state'),
            $subject_operator = $form.find('input#subject_and'),
            $since = $form.find('select#created'),
            $published = $form.find('input#Published'),
            $published_before = $form.find('input#Published_before'),
            $published_in = $form.find('input#Published_in'),
            $published_after = $form.find('input#Published_after'),
            $current = $form.find('input#CurrentFolderOnly'),
            $button = $form.find('input[type=submit]'),
            $sort = $form.find('select#sort_on'),
            options = $options.hasClass('expanded'),
            previous = null,
            timeout = null;

        var scroll_search = function(index) {
            if (timeout !== null) {
                clearTimeout(timeout);
                timeout = null;
            };
            search.scroll(index);
        };

        var schedule_search = function(force) {
            if (timeout !== null) {
                clearTimeout(timeout);
            };

            var search_term = $query.val();

            if (search_term.length == 0) {
                $('#search-results-target').html('');
                $('.listingBar').hide();
                return;
            }

            timeout = setTimeout(function () {
                var query = {term: search_term},
                    meta_types = [];

                if ($current.is(":checked")) {
                    query['url'] = $current.val();
                };

                var meta_types = [];
                $('.portal_type:checked').each(function () {
                    var $field = $(this);
                    if ($field.is(':checked')) {
                        meta_types.push($field.val());
                    };
                });
                if (meta_types.length) {
                    query['meta_type'] = meta_types;
                } else {
                    query['meta_type'] = ["does.not.exist"]; 
                };

                var languages = $('#languages').val();
                if (languages != null) {
                    query['language'] = languages;
                }

                var review_states = $('#review_state').val();
                if (review_states != null) {
                    query['review_state'] = review_states;
                }

                var subjects = $subject.val();
                if (subjects != null) {
                    query['subject'] = subjects; 
                    query['subject_and'] = $subject_operator.is(":checked");
                }

                /* Query analyzer: we need to translate the current language
                 * in Plone into the related analyzer language of Elasticsearch
                 */
                //if (CURRENT_LANGUAGE in LANG_TO_ES) 
                //    query['query_analyzer'] = LANG_TO_ES[CURRENT_LANGUAGE];
                //else
                //    query['query_analyzer'] = 'english';
                
                query['sort'] = $sort.val();
                if (options) {
                    query['contributors'] = $author.val();
                    query['created'] = $since.val();
                    query['published_year'] = validate_year($published.val());
                    query['published_before'] = $published_before.is(":checked");
                    query['published_in'] = $published_in.is(":checked");
                    query['published_after'] = $published_after.is(":checked");
                
                    $meta_types.each(function () {
                        var $field = $(this);
                        if ($field.is(':checked')) {
                            meta_types.push($field.val());
                        };
                    });
                    if (meta_types.length) {
                        query['meta_type'] = meta_types;
                    };
                };

                if (force || query.term != previous && !options) {
                    search.search(query);
                    previous = query.term;
                };
                timeout = null;
            }, 300);
        };

        search.subscribe(SummaryDisplayPlugin($summaryBox));
        search.subscribe(ResultHeaderDisplayPlugin($resultHeader));
        search.subscribe(ResultDisplayPlugin($searchResults, $emptyResults));
        search.subscribe(BatchDisplayPlugin($listingBar, scroll_search));

        $form.find('p.esSearchOptions a').bind('click', function (event) {
            options = !options;
            $options.slideToggle();
            event.preventDefault();
        });

        $button.bind('click', function(event) {
            schedule_search(true);
            event.preventDefault();
        });

        $(document).on('click', '#esSubmitSearch', schedule_search);

        $options.delegate('input,select', 'change', schedule_search);
        $query.bind('change', schedule_search);
        $query.bind('keyup', schedule_search);
        if ($query.val() || options) {
            if (options) {
                $options.show();
                $options.removeClass('expanded');
            };
            schedule_search(true);
        };
    });
};


$(document).ready(function() {

    /* Replace portal search box with our own markup */
    $('#portal-searchbox').empty();
    $('#portal-searchbox').html('<div id="es-searchbox"><a class="search-popup-trigger" href="es-searchform" id="toggle-es-search-form"></a></div>');

    /* Search popup */
    $('.search-popup-trigger').each(function(i) {
        var anchor = $(this);
        anchor.prepOverlay({     
            subtype: 'ajax',
            width: '90%',
        });
    });
});
