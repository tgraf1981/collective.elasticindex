# -*- coding: utf-8 -*-

#################################################################
# collective.elasticindex
# Initial release by Infra, Godefroid Chapelle
# Forked by ZOPYX, Andreas Jung
#################################################################


import time
import json
import random
import requests
import pkg_resources


import plone.api
from Acquisition import aq_base
from Products.CMFCore.utils import _getAuthenticatedUser
from Products.Five.browser import BrowserView
from plone.memoize import ram
from requests.auth import HTTPBasicAuth
from zope.component import getUtility
from zope.traversing.browser import absoluteURL

from plone.registry.interfaces import IRegistry
from collective.elasticindex.interfaces import IElasticSettings

from collective.elasticindex import utils
from collective.elasticindex.logger import LOG


class SearchPage(BrowserView):

    @property
    def settings(self):
        registry = getUtility(IRegistry)
        return registry.forInterface(IElasticSettings)

    @property
    def portal_url(self):
        return plone.api.portal.get().absolute_url()

    def update(self):
        settings = self.settings
        if settings.public_through_plone:
            urls = [absoluteURL(self.context, self.request) + '/search.json']
        else:
            urls = self.settings.public_server_urls
            if not urls:
                raise ValueError(u'No public ES URLs configured')

        self.server_urls = json.dumps(urls)
        self.expanded = ''
        if 'advanced' in self.request.form:
            self.expanded = 'expanded'
        self.request.set('disable_border', 1)
        self.request.set('disable_plone.leftcolumn', 1)
        self.request.set('disable_plone.rightcolumn', 1)

    def __call__(self):
        self.update()
        if not self.settings.public_through_plone:
            self.request.response.setHeader('Access-Control-Allow-Origin', '*')
            self.request.response.setHeader('Access-Control-Allow-Methods', 'POST')
            self.request.response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type')
        return super(SearchPage, self).__call__()

    def es_results_template(self):
        parts = self.settings.es_results_template.rsplit('.', 2)
        return pkg_resources.resource_string(parts[0], '.'.join(parts[1:]))

    def supported_langs(self):
        return utils.supported_langs()

    def preferred_language(self):
        return utils.preferred_language()


def cache_user(method):

    def get_cache_key(method, self, user):
        return '#'.join((user.getId() or '', str(time.time() // (5 * 60 * 60))))

    return ram.cache(get_cache_key)(method)


class SearchQuery(BrowserView):

    def get_search_urls(self):
        settings = self.settings
        index_name = settings.index_name or settings.search_index_name
        return map(lambda u: '/'.join((u, index_name, '_search')),
                   settings.public_server_urls or settings.server_urls)

    @property
    def settings(self):
        registry = getUtility(IRegistry)
        return registry.forInterface(IElasticSettings)

    @cache_user
    def _listAllowedRolesAndUsers(self, user):
        """Makes sure the list includes the user's groups.
        """
        result = user.getRoles()
        if 'Anonymous' in result:
            # The anonymous user has no further roles
            return ['Anonymous']
        result = list(result)
        if hasattr(aq_base(user), 'getGroups'):
            groups = ['user:%s' % x for x in user.getGroups()]
            if groups:
                result = result + groups
        result.append('Anonymous')
        result.append('user:%s' % user.getId())
        return result

    def __call__(self):

        if self.request.method != 'POST':
            self.request.response.setStatus(405)
            return ''
        self.request.stdin.seek(0, 0)
        payload = json.load(self.request.stdin)
        if not isinstance(payload, dict):
            self.request.response.setStatus(400)
            return ''
        if 'fields' in payload:
            if (not isinstance(payload['fields'], list) or
                    'contents' in payload['fields']):
                # Prevent people to retrieve the fulltext.
                self.request.response.setStatus(400)
                return ''
        authorizedFilter = {
            'terms': {
                'authorizedUsers': self._listAllowedRolesAndUsers(
                    _getAuthenticatedUser(self.context)),
                }}
        payload['query']['bool']['filter'].append(authorizedFilter)
        LOG.info('Query ({})'.format(json.dumps(payload, indent=4)))
        search_payload = json.dumps(payload)

        if (self.settings.server_username == '') or self.settings.server_username is None:
            response = requests.post(
                random.choice(self.get_search_urls()),
                data=search_payload, headers={'Content-Type': 'application/json'})
        else:
            response = requests.post(
                random.choice(self.get_search_urls()),
                auth=HTTPBasicAuth(self.settings.server_username, self.settings.server_password),
                data=search_payload, headers={'Content-Type': 'application/json'})
        if response.status_code == 200:
            self.request.response.setHeader(
                'Content-Type',
                'application/json;charset=UTF-8')
            return response.text
        self.request.response.setStatus(500)
        return response.text

