# -*- coding: utf-8 -*-

#################################################################
# collective.elasticindex
# Initial release by Infra, Godefroid Chapelle
# Forked by ZOPYX, Andreas Jung
#################################################################


from Products.statusmessages.interfaces import IStatusMessage

from z3c.form import button
from zope.component import getUtility
from zope.i18nmessageid import MessageFactory
from plone.registry.interfaces import IRegistry
from plone.app.registry.browser import controlpanel

from collective.elasticindex.interfaces import IElasticSettings
from collective.elasticindex.changes import changes
from collective.elasticindex.logger import LOG 
from collective.elasticindex.utils import create_index, delete_index


_ = MessageFactory('collective.elasticindex')


class ElasticSettingsEditForm(controlpanel.RegistryEditForm):

    schema = IElasticSettings
    label = _(u'Elasticsearch index settings')
    description = _(u'')

    @button.buttonAndHandler(_('Save'), name=None)
    def handleSave(self, action):
        self.save()

    @button.buttonAndHandler(_('Cancel'), name='cancel')
    def handleCancel(self, action):
        super(ElasticSettingsEditForm, self).handleCancel(self, action)

    def save(self):
        data, errors = self.extractData()
        if errors:
            self.status = self.formErrorsMessage
            return False
        self.applyChanges(data)
        return True

    @property
    def settings(self):
        registry = getUtility(IRegistry)
        return registry.forInterface(IElasticSettings)

    @button.buttonAndHandler(_(u"Create index"), name='create_index')
    def create_index(self, action):
        send = IStatusMessage(self.request).add
        try:
            create_index(self.settings)
        except Exception as e:
            LOG.error(e, exc_info=True)
            send(
                "Error while creating the index. ({})".format(e), type='error')
        else:
            send("Index created.")

    @button.buttonAndHandler(_(u"Delete index"), name='delete_index')
    def delete_index(self, action):
        send = IStatusMessage(self.request).add
        try:
            delete_index(self.settings)
        except Exception as e:
            LOG.error(e, exc_info=True)
            send(
                "Error while deleting the index. ({})".format(e), type='error')
        else:
            send("Index deleted.")

    @button.buttonAndHandler(_(u"Import site content"), name='import_content')
    def import_content(self, action):
        send = IStatusMessage(self.request).add
        try:
            changes.verify_and_index_container(self.context)
        except Exception as e:
            LOG.error(e, exc_info=True)
            send(
                "Error while indexing the index. ({})".format(e), type='error')
        else:
            send("Index refreshed.")


class ElasticIndexSettings(controlpanel.ControlPanelFormWrapper):

    form = ElasticSettingsEditForm
