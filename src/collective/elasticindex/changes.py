# -*- coding: utf-8 -*-

#################################################################
# collective.elasticindex
# Initial release by Infra, Godefroid Chapelle
# Forked by ZOPYX, Andreas Jung
#################################################################


import re
import time
import threading
import urlparse

from AccessControl.PermissionRole import rolesForPermissionOn
from Acquisition import aq_base
from Products.CMFCore.CatalogTool import _mergedLocalRoles
from Products.CMFCore.interfaces import IFolderish, IContentish
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.interfaces.siteroot import IPloneSiteRoot
from Products.CMFPlone.utils import safe_unicode
from Products.CMFCore.WorkflowCore import WorkflowException

import plone.api
from zope.component import getUtility
from zope.interface import implements
from plone.registry.interfaces import IRegistry
from plone.i18n.normalizer.base import mapUnicode
import transaction
from transaction.interfaces import ISavepointDataManager, IDataManagerSavepoint

from collective.elasticindex.interfaces import IElasticSettings
from collective.elasticindex.utils import connect
from collective.elasticindex.logger import LOG


num_sort_regex = re.compile('\d+')


def safe_unicode(s):
    if s is None:
        return None
    if isinstance(s, unicode):
        return s
    return unicode(s, 'utf-8', 'ignore')


def sortable_string(string):
    return num_sort_regex.sub(
        lambda m: m.group().zfill(6),
        mapUnicode(safe_unicode(string)).lower().strip())


def get_uid(content):
    """Return content identifier to use in ES.
    """
    if IPloneSiteRoot.providedBy(content):
        uid = 'root'
    else:
        uid = content.UID()
    return uid or None


def get_security(content):
    """Return a list of roles and users with View permission.
    Used to filter out items you're not allowed to see.
    """
    allowed = set(rolesForPermissionOn('View', content))
    # shortcut roles and only index the most basic system role if the object
    # is viewable by either of those
    if 'Anonymous' in allowed:
        return ['Anonymous']
    elif 'Authenticated' in allowed:
        return ['Authenticated']
    try:
        acl_users = getToolByName(content, 'acl_users', None)
        if acl_users is not None:
            local_roles = acl_users._getAllLocalRoles(content)
    except AttributeError:
        local_roles = _mergedLocalRoles(content)
    for user, roles in local_roles.items():
        for role in roles:
            if role in allowed:
                allowed.add('user:' + user)
    if 'Owner' in allowed:
        allowed.remove('Owner')
    return list(allowed)


def get_data(content, security=False, replace_urls=[], domain=None):
    """Return data to index in ES.
    """

    uid = get_uid(content)
    if not uid:
        return None, None
    title = content.Title()
    try:
        text = content.SearchableText()
    except:
        text = title
    url = content.absolute_url()
    if domain:
        for replace_url in replace_urls:
            if url.startswith(replace_url):
                url = url.replace(replace_url, domain)


    # lookup fullname of author
    author = content.Creator()
    authorName = None
    if author:
        user = plone.api.user.get(userid=author)
        try:
            authorName = user.aq_inner.aq_base.Title
        except AttributeError:
            authorName = author

    metaTypeName = content.portal_types[content.portal_type].Title()

    try:
        review_state = plone.api.content.get_state(obj=content)
    except (ValueError, WorkflowException):
        review_state = None

    data = {'title': title,
            'metaType': content.portal_type,
            'metaTypeName': metaTypeName,
            'sortableTitle': sortable_string(title),
            'description': safe_unicode(content.Description()),
            'language': content.Language(),
            'subject': content.Subject(),
            'contributors': content.Contributors(),
            'review_state': review_state,
            'url': url,
            'author': content.Creator(),
            'authorName': authorName,
            'content': safe_unicode(text)}

    if security:
        data['authorizedUsers'] = get_security(content)

    if hasattr(aq_base(content), 'pub_date_year'):
        data['publishedYear'] = getattr(content, 'pub_date_year')

    created = content.created()
    if created is not (None, 'None'):
        data['created'] = created.strftime('%Y-%m-%dT%H:%M:%S')

    modified = content.modified()
    if modified is not (None, 'None'):
        data['modified'] = modified.strftime('%Y-%m-%dT%H:%M:%S')

    return uid, data


def list_content(content, callback):
    """Recursively list CMF content out of the given one. ``callback``
    is called every thousand items after a commit.
    """

    def recurse(content):
        for child in content.contentValues():
            if IFolderish.providedBy(child):
                for grandchild in recurse(child):
                    yield grandchild
            yield child

    count = 0
    total = 0
    ts = time.time()
    LOG.info('Start indexing')    
    if IFolderish.providedBy(content):
        for child in recurse(content):
            yield child
            count += 1
            total += 1
            transaction.commit()
            if count == 100:
                LOG.info('{0} items indexed'.format(total))
                transaction.commit()
                content._p_jar.cacheGC()
                callback()
                count = 0
        yield content
    elif IContentish.providedBy(content):
        yield content
    LOG.info('End indexing ({} items, {:2.0f} seconds)'.format(total, time.time() - ts))    


class ElasticSavepoint(object):
    implements(IDataManagerSavepoint)

    def __init__(self, manager, index, unindex):
        self.manager = manager
        self._index = index.copy()
        self._unindex = set(unindex)

    def rollback(self):
        self.manager._index = self._index
        self.manager._unindex = self._unindex


class ElasticChanges(threading.local):
    implements(ISavepointDataManager)

    def __init__(self, manager):
        self.manager = manager
        self._clear()

    def _clear(self):
        self._index = dict()
        self._unindex = set()
        self._connection = None
        self._get_status = None

    @property
    def settings(self):
        registry = getUtility(IRegistry)
        return registry.forInterface(IElasticSettings)

    def _is_activated(self):
        transaction = self.manager.get()
        transaction.join(self)
        return True

    @property
    def only_published(self):
        try:
            settings = self.settings
        except Exception as e:
            LOG.error('Unable to retrieve collective.elasticindex setting', exc_info=True)
            return
            
        if settings is None:
            return False
        return settings.only_published

    def should_index_content(self, content):
        if self._get_status is None:
            return True
        return self._get_status(
            content, 'review_state', default='nope') == 'published'

    def should_index_container(self, contents):
        for content in contents:
            if (self._get_status is None or
                self._get_status(
                    content, 'review_state', default='nope') == 'published'):
                yield content

    def verify_and_index_container(self, content):
        if not self._is_activated():
            return

        for item in self.should_index_container(list_content(content, self._is_activated)):
            uid, data = get_data(item, security=self.settings.index_security,
                                 replace_urls=self.settings.replace_server_urls,
                                 domain=self.settings.normalize_domain_name)
            if data:
                if uid in self._unindex:
                    self._unindex.remove(uid)
                self._index[uid] = data

    def index_content(self, content):
        if not self._is_activated():
            return
        try:
            settings = self.settings
        except Exception as e:
            LOG.error('Unable to retrieve collective.elasticindex setting', exc_info=True)
            return

        uid, data = get_data(content, security=settings.index_security,
                             replace_urls=self.settings.replace_server_urls,
                             domain=settings.normalize_domain_name)
        if data:
            if uid in self._unindex:
                self._unindex.remove(uid)
            self._index[uid] = data

    def unindex_content(self, content):
        uid = get_uid(content)
        if uid in self._index:
            del self._index[uid]
        self._unindex.add(uid)

    def tpc_vote(self, transaction):
        if self._index or self._unindex:
            settings = self.settings
            if settings.server_urls:
                self._connection = connect(settings.server_urls, (settings.server_username, settings.server_password))

    def tpc_finish(self, transaction):
        if self._connection is not None:
            settings = self.settings
            for uid, data in self._index.iteritems():
                language = data['language']
                docname = 'document' if not language else 'document_{}'.format(language)
                try:
                    self._connection.index(
                        data,
                        settings.index_name,
                        docname,
                        id=uid,
                        bulk=True)
                except Exception as e:
                    msg = 'Error while indexing document {0} in Elasticsearch'.format(
                        uid)
                    LOG.error(msg, exc_info=True)
            for uid in self._unindex:
                language = data['language']
                docname = 'document' if not language else 'document_{}'.format(language)
                try:
                    self._connection.delete(
                        settings.index_name,
                        docname,
                        uid,
                        bulk=True)
                except Exception as e:
                    msg = 'Error while unindexing document {0} in Elasticsearch'.format(uid)
                    LOG.error(msg, exc_info=True)
            if self._index or self._unindex:
                try:
                    self._connection.flush_bulk(True)
                except Exception as e:
                    LOG.error('Error while flushing changes to Elasticsearch', exc_info=True)
        self._clear()

    def tpc_abort(self, transaction):
        self._clear()

    def savepoint(self):
        return ElasticSavepoint(self, self._index, self._unindex)

    def commit(self, transaction):
        pass

    def sortKey(self):
        return 'Z' * 100

    def abort(self, transaction):
        self._clear()

    def tpc_begin(self, transaction):
        pass

changes = ElasticChanges(transaction.manager)
