# -*- coding: utf-8 -*-*

#################################################################
# collective.elasticindex
# Initial release by Infra, Godefroid Chapelle
# Forked by ZOPYX, Andreas Jung
#################################################################


from zope import schema
from zope.i18nmessageid import MessageFactory
from zope.interface import Interface

_ = MessageFactory('collective.elasticindex')


class IElasticSettings(Interface):

    only_published = schema.Bool(
        title=_(u'Index only published content?'),
        default=False)

    index_security = schema.Bool(
        title=_(u'Index security?'),
        default=True)

    replace_server_urls = schema.List(
        title=_(u"URLs to replace"),
        description=_(u"List of URLs which should be replaced by the normalized domain name"),
        value_type=schema.URI(),
        required=True)


    normalize_domain_name = schema.TextLine(
        title=_(u"Normalize domain name"),
        description=_(u"If specified, replace the domain name in documents "
                      u"URL with this one."),
        required=False)

    index_name = schema.TextLine(
        title=_(u"Index name"),
        description=_(u"Elastic-Search index name where to index and search "
                      u"for documents."),
        default=u'plone',
        required=True)





    es_properties = schema.TextLine(
        title=_(u"Index schema"),
        description=_(u"Dotted name to Elastic-Search schema JSON file"),
        default=u'collective.elasticindex.es_properties.json',
        required=True)

    es_results_template = schema.TextLine(
        title=_(u"Search results template"),
        description=_(u"Dotted name to ES search results template"),
        default=u'collective.elasticindex.es_searchresults.txt',
        required=True)

    server_urls = schema.List(
        title=_(u"Server URLs"),
        description=_(u"URLs to contact Elastic-Search servers"),
        value_type=schema.URI(),
        required=True)

    server_username = schema.TextLine(
        title=_(u"Server username"),
        description=_(u"Username to log on at elasticsearch server"),
        default=u'',
        required=False)

    server_password = schema.TextLine(
        title=_(u"Server password"),
        description=_(u"Password to log on at elasticsearch server"),
        default=u'',
        required=False)

    search_index_name = schema.TextLine(
        title=_(u"Index name for search"),
        description=_(u"In certain situations you query an different index than you index to. Ex. if you have an alias or a wildcard index when you search in different plone sites at the same time."),
        default=u'',
        required=False)

    public_server_urls = schema.List(
        title=_(u"Server URLs to use for the public search"),
        description=_(u"URLs for the public search to contact Elastic-Search. "
                      u"If not specified regular server URLs will be used."),
        value_type=schema.URI(),
        required=False)

    public_through_plone = schema.Bool(
        title=_(u"Proxy search requests through Plone and apply "
                u"security filter?"),
        description=_(u"Check this and index security if you want to "
                      u"have a private search"),
        default=True)
