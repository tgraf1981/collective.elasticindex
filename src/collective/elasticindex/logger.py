# -*- coding: utf-8 -*-

#################################################################
# collective.elasticindex
# Initial release by Infra, Godefroid Chapelle
# Forked by ZOPYX, Andreas Jung
#################################################################

import logging

LOG = logging.getLogger('collective.elasticindex')

requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.WARNING)
urllib3_log = logging.getLogger("urllib3")
urllib3_log.setLevel(logging.WARNING)
