# -*- coding: utf-8 -*-

#################################################################
# collective.elasticindex
# Initial release by Infra, Godefroid Chapelle
# Forked by ZOPYX, Andreas Jung
#################################################################

import pyes
import json
import urlparse
import pkg_resources

from zope.component import getUtility
import plone.api


# Map Plone language codes to Elasticsearch language (for analyzers)

LANG_TO_ES = {
    'ar': 'arabic',
    'eu': 'basque',
    'br': 'brazilian',
    'bg': 'bulgarian',
    'cn': 'chinese',
    'da': 'danish',
    'nl': 'dutch',
    'en': 'english',
    'fi': 'finnish',
    'fr': 'french',
    'de': 'german',
    'gr': 'greek',
    'hi': 'hindi',
    'hu': 'hungarian',
    'id': 'indonesian',
    'ga': 'irish',
    'it': 'italian',
    'lv': 'latvian',
    'no': 'norwegian',
    'fa': 'persian',
    'po': 'portuguese',
    'ro': 'romanian',
    'ru': 'russian',
    'ku': 'sorani',
    'es': 'spanish',
    'sv': 'swedish',
    'tr': 'turkish',
    'th': 'thai'
}


def supported_langs():

    try:
        from plone.i18n.interfaces import ILanguageUtility
        lt = getUtility(ILanguageUtility)
        return lt.getSupportedLanguages()
    except ImportError:
        lt =  plone.api.portal.get_tool('portal_languages')
        return lt.supported_langs


def preferred_language():

    try:
        from plone.i18n.interfaces import ILanguageUtility
        lt = getUtility(ILanguageUtility)
        return lt.getPreferredLanguage()
    except ImportError:
        lt =  plone.api.portal.get_tool('portal_languages')
        return lt.getPreferredLanguage()


def parse_url(url):
    info = urlparse.urlparse(url)
    if ':' in info.netloc:
        url, port = info.netloc.split(':', 1)
    else:
        port = 80
        if info.scheme == 'https':
            port = 443
        url = info.netloc
    return 'http', url, int(port)


def connect(urls, auth):
    pyes.ES(server=map(parse_url, urls), basic_auth={'username':auth[0], 'password':auth[1]})
    try:
        return pyes.ES(server=map(parse_url, urls), basic_auth={'username':auth[0], 'password':auth[1]})
    except:
        raise ValueError('Cannot connect to servers')


def create_index(settings):

    connection = connect(settings.server_urls, (settings.server_username, settings.server_password))
    connection.indices.create_index_if_missing(settings.index_name)

    es_parts = settings.es_properties.rsplit('.', 2)
    data = pkg_resources.resource_string(es_parts[0], '.'.join(es_parts[1:]))
    properties = json.loads(data)

    langs = [None] + supported_langs()
    for lang in langs:
        docname = 'document' if lang is None else 'document_{}'.format(lang)
        for name in properties:
            if 'analyzer' in properties[name]:
                properties[name]['analyzer'] = LANG_TO_ES.get(lang, 'english')
        connection.indices.put_mapping(
            docname, {'properties': properties}, [settings.index_name])

def delete_index(settings):
    
    connection = connect(settings.server_urls, (settings.server_username, settings.server_password))
    connection.indices.delete_index_if_exists(settings.index_name)
